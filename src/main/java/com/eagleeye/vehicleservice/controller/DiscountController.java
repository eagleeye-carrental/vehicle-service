package com.eagleeye.vehicleservice.controller;

import com.eagleeye.vehicleservice.errorDto.DiscountError;
import com.eagleeye.vehicleservice.service.DiscountService;
import com.eagleeye.vehicleservice.validation.DiscountValidation;
import commonentity.responseDto.RestResponse;
import commonentity.vehicle.DiscountDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/vehicle/discount")
public class DiscountController {
    private final Logger logger = LoggerFactory.getLogger(DiscountController.class);
    private final DiscountService discountService;
    private final DiscountValidation discountValidation;

    public DiscountController(DiscountService discountService, DiscountValidation discountValidation) {
        this.discountService = discountService;
        this.discountValidation = discountValidation;
    }

    @PostMapping("/addnew")
    @PreAuthorize("hasAnyRole('ROLE_admin','ROLE_manager')")
    public ResponseEntity<RestResponse> createDiscount(@Valid @RequestBody DiscountDto discountDto) {
        RestResponse restResponse = null;
        URI uri = null;
        try {
//            DiscountError discountError = discountValidation.discountValidation(discountDto);
//            if (discountError.isValid()) {
            Optional<DiscountDto> discount = discountService.newDiscount(discountDto);
            if (discount.isPresent()) {
                uri = MvcUriComponentsBuilder.fromController(getClass()).path("/{id}/discounts")
                        .buildAndExpand(discount.get().getId())
                        .toUri();
                restResponse = new RestResponse("Data Save Successful", discount.get());
//            } else {
//                restResponse = new RestResponse("Validation Error!", discountError);
            }
        } catch (Exception e) {
            logger.debug("Exception " + e);
            restResponse = new RestResponse("DATA SAVING ERROR", new RestResponse());
            return new ResponseEntity<RestResponse>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.created(uri).body(restResponse);
    }

    @GetMapping("/{code}/codes")
    @PreAuthorize("hasAnyRole('ROLE_admin','ROLE_manager')")
    public ResponseEntity<RestResponse> discountByCode(@PathVariable("code") String code) {
        RestResponse restResponse = null;
        try {
            Optional<DiscountDto> discount = null;
            if (code != null
                    ||
                    !code.isEmpty()) {
                discount = discountService.discountByCode(code);
            }
            if (discount.isPresent()) {
                restResponse = new RestResponse("Discount Code", discount);
            } else {
                restResponse = new RestResponse("Discount Not Found!", new DiscountDto());

            }
        } catch (Exception e) {
            logger.debug("Error " + e);
            restResponse = new RestResponse("Server Error", e);
            return new ResponseEntity<RestResponse>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<RestResponse>(restResponse, HttpStatus.OK);
    }

    @GetMapping("/{id}/discounts")
    @PreAuthorize("hasAnyRole('ROLE_admin','ROLE_manager')")
    public ResponseEntity<RestResponse> discountById(@PathVariable("id") String id) {
        RestResponse restResponse = null;
        try {
            Optional<DiscountDto> discount = null;
            if (id != null
                    ||
                    !id.isEmpty()) {
                discount = discountService.discountById(id);
            }
            if (discount.isPresent()) {
                restResponse = new RestResponse("Discount Code", discount);
            } else {
                restResponse = new RestResponse("Discount Not Found!", new DiscountDto());

            }
        } catch (Exception e) {
            logger.debug("Error " + e);
            restResponse = new RestResponse("Server Error", e);
            return new ResponseEntity<RestResponse>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<RestResponse>(restResponse, HttpStatus.OK);
    }

    @GetMapping("/")
    @PreAuthorize("hasAnyRole('ROLE_admin','ROLE_manager')")
    public ResponseEntity<RestResponse> discounts() {
        RestResponse restResponse = null;
        try {
            List<DiscountDto> discountCoupons = discountService.discounts();
            if (discountCoupons.size() > 0) {
                restResponse = new RestResponse("Discount Coupon List", discountCoupons);
            } else {
                restResponse = new RestResponse("No Discount Coupon Found!", new DiscountDto());
                return new ResponseEntity<RestResponse>(restResponse, HttpStatus.NOT_FOUND);
            }
            return new ResponseEntity<RestResponse>(restResponse, HttpStatus.OK);
        } catch (Exception e) {
            logger.debug("Exception " + e);
            restResponse = new RestResponse("Internal Server Error", e);
            return new ResponseEntity<RestResponse>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}/update")
    @PreAuthorize("hasAnyRole('ROLE_admin')")
    public ResponseEntity<RestResponse> updateDiscount(@PathVariable("id") String id,
                                                       @RequestBody DiscountDto discountDto) {
        return null;
    }
}
