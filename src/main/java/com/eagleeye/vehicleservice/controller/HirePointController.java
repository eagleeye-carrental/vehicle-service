package com.eagleeye.vehicleservice.controller;

import com.eagleeye.vehicleservice.errorDto.HirePointError;
import com.eagleeye.vehicleservice.service.HirePointService;
import com.eagleeye.vehicleservice.validation.HirePointValidation;
import commonentity.responseDto.RestResponse;
import commonentity.vehicle.HirePointDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/vehicle/hirepoint")
public class HirePointController {
    private final Logger logger = LoggerFactory.getLogger(HirePointController.class);
    private final HirePointService hirePointService;
    private final HirePointValidation hirePointValidation;

    @Autowired
    public HirePointController(HirePointService hirePointService, HirePointValidation hirePointValidation) {
        this.hirePointService = hirePointService;
        this.hirePointValidation = hirePointValidation;
    }

    @PostMapping("/addnew")
    @PreAuthorize("hasAnyRole('ROLE_admin')")
    public ResponseEntity<RestResponse> createHirePoint(@Valid @RequestBody HirePointDto dto) {
        RestResponse restResponse = null;
        URI uri = null;
        try {
            HirePointError error = hirePointValidation.validator(dto);
            if (error.isValid()) {
                Optional<HirePointDto> hirepoint = hirePointService.addHirePoint(dto);

                if (hirepoint.isPresent()) {
                    restResponse = new RestResponse("hirepoint Created", hirepoint.get());
                    uri = MvcUriComponentsBuilder.fromController(getClass()).path("/{id}/hirepoint")
                            .buildAndExpand(hirepoint.get().getId())
                            .toUri();
                }
            }
            else {
                restResponse = new RestResponse("Validation Failed!",error);
                return new ResponseEntity<RestResponse>(restResponse,HttpStatus.CONFLICT);
            }

        } catch (Exception e) {
            logger.debug("Exception" + e);
            System.out.println(e);
            restResponse = new RestResponse("Internal Server Error!", e);
            return new ResponseEntity<RestResponse>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.created(uri).body(restResponse);
    }

    @GetMapping("/{id}/hirepoint")
    @PreAuthorize("hasAuthority('READ')")
    public ResponseEntity<RestResponse> hirePoint(@PathVariable("id") String id) {
        Optional<HirePointDto> hirePoint = Optional.empty();
        RestResponse restResponse = null;
        try {
            if (id != null
                    || !id.isEmpty()) {
                hirePoint = hirePointService.hirePoint(id);
            }
            if (hirePoint.isPresent()) {
                restResponse = new RestResponse("Data Found", hirePoint.get());
            } else {
                restResponse = new RestResponse("Data Not Found", hirePoint);
            }
        } catch (Exception e) {
            logger.debug("Error " + e);
            return
                    new ResponseEntity<RestResponse>(new RestResponse("Error", e),
                            HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(restResponse);
    }

    @GetMapping("/{filter}/city")
    @PreAuthorize("hasAuthority('READ')")
    public ResponseEntity<RestResponse> hirePointsByCity(@PathVariable("filter") String city) {
       return null;
    }

    @GetMapping("/")
    @PreAuthorize("hasAuthority('READ')")
    public ResponseEntity<RestResponse> hirePoints() {
        RestResponse restResponse = null;
        try {
            List<HirePointDto> hirepoints = hirePointService.hirePoints();
            if (hirepoints.size() > 0) {
                restResponse = new RestResponse("Data Found", hirepoints);
            } else {
                restResponse = new RestResponse("No Data Found!", hirepoints);
                return
                        new ResponseEntity<RestResponse>(restResponse,
                                HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            logger.debug("Error" + e);
            restResponse = new RestResponse("Error", e);
            return
                    new ResponseEntity<RestResponse>(restResponse,
                            HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(restResponse);
    }

    @PutMapping("/{id}/update")
    @PreAuthorize("hasRole('ROLE_admin')")
    public ResponseEntity<RestResponse> updateHirePoint(@PathVariable("id") String id,
                                                        HirePointDto hirePointDto) {
        return null;
    }

}
