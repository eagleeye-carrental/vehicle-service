package com.eagleeye.vehicleservice.controller;

import com.eagleeye.vehicleservice.errorDto.ManufacturerError;
import com.eagleeye.vehicleservice.service.ManufacturerService;
import com.eagleeye.vehicleservice.validation.ManufacturerValidation;
import commonentity.responseDto.RestResponse;
import commonentity.vehicle.ManufacturerDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/vehicle/manufacturer")
public class ManufacturerController {
    private final Logger logger = LoggerFactory.getLogger(ManufacturerController.class);
    private final ManufacturerValidation manufacturerValidation;
    private final ManufacturerService manufacturerService;

    @Autowired
    public ManufacturerController(ManufacturerValidation manufacturerValidation,
                                  ManufacturerService manufacturerService) {
        this.manufacturerValidation = manufacturerValidation;
        this.manufacturerService = manufacturerService;
    }

    @PostMapping("/addnew")
    @PreAuthorize("hasRole('ROLE_admin')")
    public ResponseEntity<RestResponse> createManufacturer(@Valid @RequestBody ManufacturerDto manufacturerDto) {
        RestResponse restResponse = null;
        URI uri = null;
        try {
            ManufacturerError validation = manufacturerValidation.validation(manufacturerDto);
            if (validation.isValid()) {
                Optional<ManufacturerDto> manufacturer = manufacturerService.addManufacturer(manufacturerDto);
                uri = MvcUriComponentsBuilder.fromController(getClass()).path("/{id}/manufacturer")
                        .buildAndExpand(manufacturer.get().getId())
                        .toUri();
                restResponse = new RestResponse("Data Saved Success", manufacturer.get());
            }
            else {
                restResponse = new RestResponse("Validation Failed!",validation);
                return new ResponseEntity<RestResponse>(restResponse, HttpStatus.CONFLICT);
            }
        } catch (Exception e) {
            logger.debug("Error" + e);
            restResponse = new RestResponse("Internal Server Error", e);
            return new ResponseEntity<RestResponse>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.created(uri).body(restResponse);
    }

    @GetMapping("/{id}/manufacturer")
    @PreAuthorize("hasAuthority('READ')")
    public ResponseEntity<RestResponse> manufacturer(@PathVariable("id") String id) {
        Optional<ManufacturerDto> manufacturer = Optional.empty();
        RestResponse restResponse = null;
        try {
            manufacturer = manufacturerService.findById(id);
            if (manufacturer.isPresent()) {
                restResponse = new RestResponse("Data Found", manufacturer.get());
            } else {
                restResponse = new RestResponse("Data Not Found!", manufacturer);
            }

        } catch (Exception e) {
            logger.debug("Error" + e);
            restResponse = new RestResponse("Internal server Error", e);
            return new ResponseEntity<RestResponse>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(restResponse);
    }

    @GetMapping("/{name}")
    @PreAuthorize("hasAuthority('READ')")
    public ResponseEntity<RestResponse> manufacturerByName(@PathVariable("name") String name) {
        Optional<ManufacturerDto> manufacturer = Optional.empty();
        RestResponse restResponse = null;
        try {
            manufacturer = manufacturerService.findByName(name);
            if (manufacturer.isPresent()) {
                restResponse = new RestResponse("Data Found", manufacturer.get());
            } else {
                restResponse = new RestResponse("Data Not Found!", manufacturer);
            }

        } catch (Exception e) {
            logger.debug("Error" + e);
            restResponse = new RestResponse("Internal server Error", e);
            return new ResponseEntity<RestResponse>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(restResponse);
    }

    @GetMapping("/")
    @PreAuthorize("hasAuthority('READ')")
    public ResponseEntity<RestResponse> manufacturers() {
        List<ManufacturerDto> manufacturers;
        RestResponse restResponse = null;
        try {
            manufacturers = manufacturerService.manufacturers();
            if (manufacturers != null) {
                restResponse = new RestResponse("Data Found", manufacturers);
            } else {
                restResponse = new RestResponse("Data Not Found!", manufacturers);
            }
        } catch (Exception e) {
            logger.debug("Error" + e);
            restResponse = new RestResponse("Internal server Error", e);
            return new ResponseEntity<RestResponse>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(restResponse);
    }

    @PutMapping("/{id}/update")
    @PreAuthorize("hasRole('ROLE_admin')")
    public ResponseEntity<RestResponse> updateManufacturer(@PathVariable("id") String id,
                                                           ManufacturerDto manufacturerDto) {
        return null;
    }
}
