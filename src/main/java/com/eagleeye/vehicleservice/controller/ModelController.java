package com.eagleeye.vehicleservice.controller;

import com.eagleeye.vehicleservice.errorDto.ModelError;
import com.eagleeye.vehicleservice.service.ModelService;
import com.eagleeye.vehicleservice.validation.ModelValidation;
import commonentity.responseDto.RestResponse;
import commonentity.vehicle.ModelDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/vehicle/model")
public class ModelController {
    private final ModelValidation modelValidation;
    private final ModelService modelService;
    private final Logger logger = LoggerFactory.getLogger(ModelController.class);

    public ModelController(ModelValidation modelValidation, ModelService modelService) {
        this.modelValidation = modelValidation;
        this.modelService = modelService;
    }

    @PostMapping("/addnew")
    @PreAuthorize("hasRole('ROLE_admin')")
    public ResponseEntity<RestResponse> createModel(@Valid @RequestBody ModelDto modelDto) {
        RestResponse restResponse = null;
        URI uri = null;
        try {
            if (modelDto != null) {
                Optional<ModelDto> model = Optional.empty();
                ModelError validation = modelValidation.validation(modelDto);
                if (validation.isValid()) {
                    model = modelService.addNewModel(modelDto);
                    restResponse = new RestResponse("Data Saved", model.get());
                    uri = MvcUriComponentsBuilder.fromController(getClass())
                            .path("/{id}/model")
                            .buildAndExpand(model.get().getId())
                            .toUri();
                } else {
                    restResponse = new RestResponse("Data Validation Failed!", validation);
                    return new ResponseEntity<RestResponse>(restResponse, HttpStatus.CONFLICT);
                }
            }


        } catch (Exception e) {
            logger.error("Error" + e);
            return new ResponseEntity<RestResponse>(new RestResponse(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.created(uri).body(restResponse);
    }

    @GetMapping("/{id}/model")
    @PreAuthorize("hasAuthority('READ')")
    public ResponseEntity<RestResponse> model(@PathVariable("id") String id) {
        RestResponse restResponse = null;
        try {
            Optional<ModelDto> model = modelService.findById(id);
            if (model.isPresent()) {
                restResponse = new RestResponse("Data Found!", model.get());
            }
        } catch (Exception e) {
            logger.error("Error" + e);
            return new ResponseEntity<RestResponse>(new RestResponse(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(restResponse);
    }

    @GetMapping("/")
    @PreAuthorize("hasAuthority('READ')")
    public ResponseEntity<RestResponse> models() {
        RestResponse restResponse = null;
        try {
            List<ModelDto> models = modelService.findAll();
            restResponse = new RestResponse("Data Found", models);
        } catch (Exception e) {
            logger.error("Error" + e);
            return new ResponseEntity<RestResponse>(new RestResponse(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return ResponseEntity.ok(restResponse);
    }

//    @GetMapping("/{manufacturerId}")
//    public ResponseEntity<RestResponse> modelsByManufacturerId(@PathVariable("manufacturerId")
//                                                                       String manufacturerId) {
//        RestResponse restResponse = null;
//        try {
//            List<ModelDto> model = modelService.modelByManufactureId(manufacturerId);
//
//            restResponse = new RestResponse("Data Found!", model);
//        } catch (Exception e) {
//            logger.error("Error" + e);
//            return new ResponseEntity<RestResponse>(new RestResponse(), HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//        return ResponseEntity.ok(restResponse);
//    }
//
//    @GetMapping("/{manufacturer_name}/mname")
//    public ResponseEntity<RestResponse> modelsByManufacturerName(@PathVariable("manufacturer_name")
//                                                                         String manufacturerName) {
//        RestResponse restResponse = null;
//        try {
//            List<ModelDto> model = modelService.modelByManufacturerName(manufacturerName);
//
//            restResponse = new RestResponse("Data Found!", model);
//        } catch (Exception e) {
//            logger.error("Error" + e);
//            return new ResponseEntity<RestResponse>(new RestResponse(), HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//        return ResponseEntity.ok(restResponse);
//    }

    @PutMapping("/{id}/update")
    @PreAuthorize("hasRole('ROLE_admin')")
    public ResponseEntity<RestResponse> updateModel(@PathVariable("id") String id,
                                                    ModelDto ModelDto) {
        return null;
    }
}
