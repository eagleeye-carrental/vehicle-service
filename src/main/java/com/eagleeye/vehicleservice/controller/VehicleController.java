package com.eagleeye.vehicleservice.controller;

import com.eagleeye.vehicleservice.errorDto.VehicleError;
import com.eagleeye.vehicleservice.service.VehicleService;
import com.eagleeye.vehicleservice.validation.VehicleValidation;
import commonentity.responseDto.RestResponse;
import commonentity.vehicle.VehicleDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/vehicle")
public class VehicleController {
    private final VehicleValidation vehicleValidation;
    private final VehicleService vehicleService;

    @Autowired
    public VehicleController(VehicleValidation vehicleValidation, VehicleService vehicleService) {
        this.vehicleValidation = vehicleValidation;
        this.vehicleService = vehicleService;
    }

    @PostMapping("/addnew")
    @PreAuthorize("hasAnyRole('ROLE_admin')")
    public ResponseEntity<RestResponse> createvehicle(@Valid @RequestBody VehicleDto vehicleDto) {
        RestResponse restResponse = null;
        URI uri = null;
        try {
            VehicleError validation = vehicleValidation.validator(vehicleDto.getPlateNumber());
            if (validation.isValid()) {
                VehicleDto vehicle = vehicleService.addNew(vehicleDto);
                restResponse = new RestResponse("Data Saved Successfully", vehicle);
                uri = MvcUriComponentsBuilder.fromController(getClass()).path("/{id}/discounts")
                        .buildAndExpand(vehicle.getId())
                        .toUri();
                return ResponseEntity.created(uri).body(restResponse);
            } else {
                restResponse = new RestResponse("Validation Failed!", validation);
                return new ResponseEntity<RestResponse>(restResponse, HttpStatus.CONFLICT);
            }

        } catch (Exception e) {
            return new ResponseEntity<RestResponse>(new RestResponse("Server Error", null), HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }

    @GetMapping("/{id}/id")
    @PreAuthorize("hasAuthority('READ')")
    public ResponseEntity<RestResponse> vehicleById(@PathVariable("id") String id) {
        RestResponse restResponse = null;
        try {
            Optional<VehicleDto> vehicle = vehicleService.findById(id);
            if(vehicle.isPresent()) {
                restResponse = new RestResponse("Vehicle Found", vehicle.get());
                return ResponseEntity.ok(restResponse);
            }
            else {
                restResponse = new RestResponse("Vehicle Not Found!", vehicle);
                return new ResponseEntity<RestResponse>(restResponse, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            restResponse = new RestResponse("Vehicle Not Found!", null);
            return new ResponseEntity<RestResponse>(restResponse,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
    @GetMapping("/{plate_number}/plate_number")
    @PreAuthorize("hasAuthority('READ')")
    public ResponseEntity<RestResponse> vehicleByPlateNumber(@PathVariable("plate_number") String plateNumber) {
        RestResponse restResponse = null;
        try {
            Optional<VehicleDto> vehicle = vehicleService.findVehicleByPlateNumber(plateNumber);
            if(vehicle.isPresent()) {
                restResponse = new RestResponse("Vehicle Found", vehicle.get());
                return ResponseEntity.ok(restResponse);
            }
            else {
                restResponse = new RestResponse("Vehicle Not Found!", vehicle);
                return new ResponseEntity<RestResponse>(restResponse, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            restResponse = new RestResponse("Vehicle Not Found!", null);
            return new ResponseEntity<RestResponse>(restResponse,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/")
    @PreAuthorize("hasRole('ROLE_admin')")
    public ResponseEntity<RestResponse> vehicles() {
        RestResponse restResponse = null;
        try {
            List<VehicleDto> vehicleList = vehicleService.vehicles();
            if(vehicleList.size() > 0) {
                restResponse = new RestResponse("Data Found",vehicleList);
                return ResponseEntity.ok(restResponse);
            }
            else {
                restResponse = new RestResponse("Data Not Found",vehicleList);
                return new ResponseEntity<RestResponse>(restResponse, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            restResponse = new RestResponse("Data Not Found",null);
            return new ResponseEntity<RestResponse>(restResponse, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/{id}/update")
    @PreAuthorize("hasAnyRole('ROLE_admin','ROLE_manager')")
    public ResponseEntity<RestResponse> updatevehicle(@PathVariable("id") String id,
                                                      VehicleDto vehicleDto) {
        return null;
    }

}
