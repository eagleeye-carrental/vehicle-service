package com.eagleeye.vehicleservice.document;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

@Data
@Document(collection = "discount_percentage")
public class Discount implements Serializable {
    @Id
    private String id;
    private double discountPercentage;
    private String discountCode;
    private Date discountValidity;
}
