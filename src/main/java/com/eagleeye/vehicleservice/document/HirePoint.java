package com.eagleeye.vehicleservice.document;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Document(collection = "vehicle_hirepoint")
@Data
public class HirePoint implements Serializable {
    @Id
    private String id;
    private String hirePointName;
    private String address1;
    private String address2;
    private String city;
    private String country;
    private String postcode;
    private String mobileNumber;
    private String email;
}
