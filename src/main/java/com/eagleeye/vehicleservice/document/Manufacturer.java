package com.eagleeye.vehicleservice.document;

import commonentity.vehicle.ManufacturerDto;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Data
@Document(collection = "vehicle_manufacturer")
public class Manufacturer implements Serializable {
    @Id
    private String id;
    private String manufacturerName;


}
