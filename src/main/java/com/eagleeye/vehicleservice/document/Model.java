package com.eagleeye.vehicleservice.document;

import commonentity.vehicle.ModelDto;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Data
@Document(collection = "manufacturer_model")
public class Model implements Serializable {
    @Id
    private String id;
    private Manufacturer manufacturer;
    private String modelName;

}
