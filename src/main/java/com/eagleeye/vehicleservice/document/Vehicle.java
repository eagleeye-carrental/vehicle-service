package com.eagleeye.vehicleservice.document;

import commonentity.vehicle.VehicleDto;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Data
@Document(collection = "vehicle")
public class Vehicle implements Serializable {
    @Id
    private String id;
    private String plateNumber;
    private Model vehicle;
    private Discount discount;
    private HirePoint hirePoint;


}
