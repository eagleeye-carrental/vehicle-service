package com.eagleeye.vehicleservice.errorDto;

import lombok.Data;

import java.io.Serializable;
import java.time.LocalDate;

@Data
public class DiscountError implements Serializable {
    private String discountPercentage;
    private String discountCode;
    private String discountValidity;
    private boolean valid;
}
