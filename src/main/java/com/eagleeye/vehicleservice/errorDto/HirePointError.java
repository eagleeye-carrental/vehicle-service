package com.eagleeye.vehicleservice.errorDto;

import lombok.Data;

import java.io.Serializable;

@Data
public class HirePointError implements Serializable {
    private String email;
    private String phone;
    private boolean valid;
}
