package com.eagleeye.vehicleservice.errorDto;

import lombok.Data;

import java.io.Serializable;

@Data
public class ManufacturerError implements Serializable {
    private String manufacturer;
    private boolean valid;
}
