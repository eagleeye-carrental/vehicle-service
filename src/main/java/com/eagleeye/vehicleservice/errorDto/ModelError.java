package com.eagleeye.vehicleservice.errorDto;

import lombok.Data;

import java.io.Serializable;

@Data
public class ModelError implements Serializable {
    private String modelName;
    private boolean valid;

}
