package com.eagleeye.vehicleservice.errorDto;

import lombok.Data;

import java.io.Serializable;

@Data
public class VehicleError implements Serializable {
    private String plateNumber;
    private boolean valid;
}
