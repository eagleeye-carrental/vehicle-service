package com.eagleeye.vehicleservice.mapping;

public interface IMapping<E,D> {
    E dtoToEntity(D dto);
    D entityToDto(E entity);
}
