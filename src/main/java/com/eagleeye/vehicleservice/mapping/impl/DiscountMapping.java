package com.eagleeye.vehicleservice.mapping.impl;

import com.eagleeye.vehicleservice.document.Discount;
import com.eagleeye.vehicleservice.mapping.IMapping;
import commonentity.vehicle.DiscountDto;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class DiscountMapping implements IMapping<Discount, DiscountDto> {
    private final ModelMapper modelMapper;

    public DiscountMapping(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public Discount dtoToEntity(DiscountDto dto) {
        Discount discount = new Discount();
        modelMapper.map(dto, discount);
        return discount;
    }

    @Override
    public DiscountDto entityToDto(Discount entity) {
        DiscountDto discountDto = new DiscountDto();
        modelMapper.map(entity, discountDto);
        return discountDto;
    }
}
