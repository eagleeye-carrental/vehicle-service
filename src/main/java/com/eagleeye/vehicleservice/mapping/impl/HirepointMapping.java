package com.eagleeye.vehicleservice.mapping.impl;

import com.eagleeye.vehicleservice.document.HirePoint;
import com.eagleeye.vehicleservice.mapping.IMapping;
import commonentity.vehicle.HirePointDto;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Component;

@Component
public class HirepointMapping implements IMapping<HirePoint, HirePointDto> {
    private final ModelMapper modelMapper;

    public HirepointMapping(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public HirePoint dtoToEntity(HirePointDto dto) {
        HirePoint entity = new HirePoint();
        modelMapper.map(dto, entity);
        return entity;
    }

    @Override
    public HirePointDto entityToDto(HirePoint entity) {
        HirePointDto dto = new HirePointDto();
        modelMapper.map(entity, dto);
        return dto;
    }
}
