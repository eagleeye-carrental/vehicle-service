package com.eagleeye.vehicleservice.mapping.impl;

import com.eagleeye.vehicleservice.document.Manufacturer;
import com.eagleeye.vehicleservice.mapping.IMapping;
import commonentity.vehicle.ManufacturerDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ManufacturerMapping implements IMapping<Manufacturer, ManufacturerDto> {
    private final ModelMapper modelMapper;

    @Autowired
    public ManufacturerMapping(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public Manufacturer dtoToEntity(ManufacturerDto dto) {
        Manufacturer entity = new Manufacturer();
        modelMapper.map(dto, entity);
        return entity;
    }

    @Override
    public ManufacturerDto entityToDto(Manufacturer entity) {
        ManufacturerDto dto = new ManufacturerDto();
        modelMapper.map(entity, dto);
        return dto;
    }
}
