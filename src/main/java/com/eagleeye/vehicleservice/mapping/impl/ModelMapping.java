package com.eagleeye.vehicleservice.mapping.impl;

import com.eagleeye.vehicleservice.document.Model;
import com.eagleeye.vehicleservice.mapping.IMapping;
import commonentity.vehicle.ModelDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ModelMapping implements IMapping<Model, ModelDto> {
    private final ModelMapper modelMapper;

    @Autowired
    public ModelMapping(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public Model dtoToEntity(ModelDto dto) {
        Model entity = new Model();
        modelMapper.map(dto, entity);
        return entity;
    }

    @Override
    public ModelDto entityToDto(Model entity) {
        ModelDto dto = new ModelDto();
        modelMapper.map(entity, dto);
        return dto;
    }
}
