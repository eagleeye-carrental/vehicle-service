package com.eagleeye.vehicleservice.mapping.impl;

import com.eagleeye.vehicleservice.document.Vehicle;
import com.eagleeye.vehicleservice.mapping.IMapping;
import commonentity.vehicle.VehicleDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class VehicleMapping implements IMapping<Vehicle, VehicleDto> {
    private final ModelMapper modelMapper;

    @Autowired
    public VehicleMapping(ModelMapper modelMapper) {
        this.modelMapper = modelMapper;
    }

    @Override
    public Vehicle dtoToEntity(VehicleDto dto) {
        Vehicle entity = new Vehicle();
        modelMapper.map(dto,entity);
        return entity;

    }

    @Override
    public VehicleDto entityToDto(Vehicle entity) {
        VehicleDto dto = new VehicleDto();
        modelMapper.map(entity,dto);
        return dto;
    }
}
