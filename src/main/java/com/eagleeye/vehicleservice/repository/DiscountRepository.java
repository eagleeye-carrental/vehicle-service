package com.eagleeye.vehicleservice.repository;

import com.eagleeye.vehicleservice.document.Discount;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface DiscountRepository extends MongoRepository<Discount,String> {
    Optional<Discount> findDiscountByDiscountCodeIgnoreCase(String discountCode);
}
