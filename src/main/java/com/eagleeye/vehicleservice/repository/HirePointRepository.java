package com.eagleeye.vehicleservice.repository;

import com.eagleeye.vehicleservice.document.HirePoint;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;
import java.util.Optional;

public interface HirePointRepository extends MongoRepository<HirePoint, String> {
//    @Query(value = "{city: {$regex: ?0, $options: 'i'}}")
    List<HirePoint> findByCityIgnoreCase(String city);
    Optional<HirePoint> findByEmailIgnoreCase(String email);
    Optional<HirePoint> findByMobileNumber(String mobile);
}
