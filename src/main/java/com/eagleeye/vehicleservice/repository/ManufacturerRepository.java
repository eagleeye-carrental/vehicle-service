package com.eagleeye.vehicleservice.repository;

import com.eagleeye.vehicleservice.document.Manufacturer;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.Optional;

public interface ManufacturerRepository extends MongoRepository<Manufacturer, String> {
//    @Query("{'manufacturerName': {$regex: ?0, $options: 'i'}}")
    Optional<Manufacturer> findByManufacturerNameIgnoreCase(String name);
}
