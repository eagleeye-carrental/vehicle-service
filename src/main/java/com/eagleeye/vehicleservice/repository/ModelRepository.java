package com.eagleeye.vehicleservice.repository;

import com.eagleeye.vehicleservice.document.Model;
import org.springframework.boot.Banner;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;
import java.util.Optional;

public interface ModelRepository extends MongoRepository<Model, String> {
    List<Model> findByManufacturer_Id(String manufactureId);
//    @Query(value= "{'manufacturer.manufacturerName': { $regex: '?0', $options: 'i' } }")
    List<Model> findByManufacturer_ManufacturerNameIgnoreCase(String manufactureName);
    Optional<Model> findByModelNameIgnoreCase(String modelName);
}
