package com.eagleeye.vehicleservice.repository;

import com.eagleeye.vehicleservice.document.Vehicle;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface VehicleRepository extends MongoRepository<Vehicle,String> {
    Optional<Vehicle> findByPlateNumberIgnoreCase(String plateNumber);

}
