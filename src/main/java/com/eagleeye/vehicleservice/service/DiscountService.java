package com.eagleeye.vehicleservice.service;

import commonentity.vehicle.DiscountDto;

import java.util.List;
import java.util.Optional;

public interface DiscountService {
    Optional<DiscountDto> newDiscount(DiscountDto discountDto);

    Optional<DiscountDto> discountByCode(String discountCode);

    Optional<DiscountDto> discountById(String id);

    Optional<DiscountDto> updateDiscount(String id,
                                         DiscountDto discountDto);

    List<DiscountDto> discounts();


}
