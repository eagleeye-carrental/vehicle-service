package com.eagleeye.vehicleservice.service;

import com.eagleeye.vehicleservice.document.HirePoint;
import commonentity.vehicle.HirePointDto;

import java.util.List;
import java.util.Optional;

public interface HirePointService {
    Optional<HirePointDto> addHirePoint(HirePointDto hirePointDto);

    Optional<HirePointDto> hirePoint(String hirepointId);

    List<HirePointDto> hirePoints();

    List<HirePointDto> hirepointsByCity(String city);

    Optional<HirePointDto> updateHirePoint(String id,
                                        HirePointDto hirePointDto);

    Optional<HirePointDto> findByMobile(String mobileNumber);

    Optional<HirePointDto> findByEmail(String email);
}
