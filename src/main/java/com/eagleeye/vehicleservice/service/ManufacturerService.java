package com.eagleeye.vehicleservice.service;

import commonentity.vehicle.ManufacturerDto;

import java.util.List;
import java.util.Optional;

public interface ManufacturerService {
    Optional<ManufacturerDto> addManufacturer(ManufacturerDto dto);

    Optional<ManufacturerDto> findById(String id);

    Optional<ManufacturerDto> findByName(String name);

    Optional<ManufacturerDto> updateManufacturer(String id,
                                                 ManufacturerDto dto);

    void deleteManufacturer(String id);

    List<ManufacturerDto> manufacturers();
}
