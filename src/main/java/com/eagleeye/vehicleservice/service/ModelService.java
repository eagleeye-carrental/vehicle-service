package com.eagleeye.vehicleservice.service;

import commonentity.vehicle.ModelDto;

import java.util.List;
import java.util.Optional;

public interface ModelService {
    Optional<ModelDto> addNewModel(ModelDto modelDto);

    Optional<ModelDto> findById(String id);

    List<ModelDto> modelByManufactureId(String id);

    List<ModelDto> modelByManufacturerName(String manufactureName);

    Optional<ModelDto> updateModel(String modelId,
                                   ModelDto modelDto);
    Optional<ModelDto> findByModelName(String modelName);
    List<ModelDto> findAll();


}
