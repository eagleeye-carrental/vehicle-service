package com.eagleeye.vehicleservice.service;

import com.eagleeye.vehicleservice.document.Vehicle;
import commonentity.vehicle.ManufacturerDto;
import commonentity.vehicle.ModelDto;
import commonentity.vehicle.VehicleDto;

import java.util.List;
import java.util.Optional;

public interface VehicleService {
    public VehicleDto addNew(VehicleDto vehicleDto);

    Optional<VehicleDto> findVehicleByPlateNumber(String vehiclePlateNumber);

    Optional<VehicleDto> findById(String id);
    List<VehicleDto> vehicles();

}
