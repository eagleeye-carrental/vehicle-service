package com.eagleeye.vehicleservice.service.impl;

import com.eagleeye.vehicleservice.document.Discount;
import com.eagleeye.vehicleservice.mapping.impl.DiscountMapping;
import com.eagleeye.vehicleservice.repository.DiscountRepository;
import com.eagleeye.vehicleservice.service.DiscountService;
import commonentity.vehicle.DiscountDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class DiscountServiceImpl implements DiscountService {
    private final DiscountRepository discountRepository;
    private final DiscountMapping discountMapping;

    @Autowired
    public DiscountServiceImpl(DiscountRepository discountRepository,
                               DiscountMapping discountMapping) {
        this.discountRepository = discountRepository;
        this.discountMapping = discountMapping;
    }

    @Override
    public Optional<DiscountDto> newDiscount(DiscountDto discountDto) {
        if (discountDto != null) {
            Discount discount = discountRepository
                                    .insert(discountMapping.dtoToEntity(discountDto));
            return Optional.of(discountMapping.entityToDto(discount));
        }
        return Optional.empty();
    }

    @Override
    public Optional<DiscountDto> discountByCode(String discountCode) {
        Optional<Discount> discount = null;
        if (discountCode != null || !discountCode.isEmpty()) {
            discount = discountRepository.findDiscountByDiscountCodeIgnoreCase(discountCode);
        }

        if (discount.isPresent()) {
            return Optional
                    .of(discountMapping.entityToDto(discount.get()));
        }
        return Optional.empty();
    }

    @Override
    public Optional<DiscountDto> discountById(String id) {
        Optional<Discount> discount = null;
        if (id != null || !id.isEmpty()) {
            discount = discountRepository.findById(id);
        }
        if (discount.isPresent()) {
            return Optional
                    .of(discountMapping.entityToDto(discount.get()));
        }
        return Optional.empty();
    }

    @Override
    public Optional<DiscountDto> updateDiscount(String id, DiscountDto discountDto) {
//        Optional<Discount> discount = null;
//        if (id != null || !id.isEmpty()) {
//           discount = discountRepository.findById(id);
//        }
//        if(discount.isPresent()) {
//            discount
//                    .map(existingDiscount -> {
//                        existingDiscount.setDiscountCode(discountDto.getDiscountCode());
//                        existingDiscount.setDiscountPercentage(discountDto.getDiscountPercentage());
//                        existingDiscount.setDiscountValidity(discountDto.getDiscountValidity());
//                        return discountRepository.save(existingDiscount);
//                    })
//        }
        //todo:: update discount
        return Optional.empty();
    }

    @Override
    public List<DiscountDto> discounts() {
        return discountRepository.findAll()
                .stream()
                .map(discountMapping::entityToDto)
                .collect(Collectors.toList());
    }
}
