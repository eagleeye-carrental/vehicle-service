package com.eagleeye.vehicleservice.service.impl;

import com.eagleeye.vehicleservice.document.HirePoint;
import com.eagleeye.vehicleservice.mapping.impl.HirepointMapping;
import com.eagleeye.vehicleservice.repository.HirePointRepository;
import com.eagleeye.vehicleservice.service.HirePointService;
import commonentity.vehicle.HirePointDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class HirePointServiceImpl implements HirePointService {
    private final HirePointRepository hirePointRepository;
    private final HirepointMapping hirepointMapping;

    @Autowired
    public HirePointServiceImpl(HirePointRepository hirePointRepository,
                                HirepointMapping hirepointMapping) {
        this.hirePointRepository = hirePointRepository;
        this.hirepointMapping = hirepointMapping;
    }

    @Override
    public Optional<HirePointDto> addHirePoint(HirePointDto hirePointDto) {

        HirePoint hirePoint = hirePointRepository
                .insert(hirepointMapping.dtoToEntity(hirePointDto));
        if (hirePoint != null) {
            return Optional.of(hirepointMapping.entityToDto(hirePoint));
        }
        return Optional.empty();
    }

    @Override
    public Optional<HirePointDto> hirePoint(String hirepointId) {
        Optional<HirePoint> hirePoint = hirePointRepository.findById(hirepointId);
        if (hirePoint.isPresent()) {
            return Optional.of(hirepointMapping.entityToDto(hirePoint.get()));
        }
        return Optional.empty();
    }

    @Override
    public List<HirePointDto> hirePoints() {
        List<HirePoint> hirePoints = hirePointRepository.findAll();
        return hirePoints
                .stream()
                .map(hirepointMapping::entityToDto)
                .collect(Collectors.toList());

    }

    @Override
    public List<HirePointDto> hirepointsByCity(String city) {
        List<HirePoint> hirePoints_city = hirePointRepository.findByCityIgnoreCase(city);
        return hirePoints_city
                .stream()
                .map(hirepointMapping::entityToDto)
                .collect(Collectors.toList());
    }

    @Override
    public Optional<HirePointDto> updateHirePoint(String id, HirePointDto hirePointDto) {
        //todo:: update hirepoint
        return Optional.empty();
    }

    @Override
    public Optional<HirePointDto> findByMobile(String mobileNumber) {
        Optional<HirePoint> hirepoint = hirePointRepository.findByMobileNumber(mobileNumber);
        if (hirepoint.isPresent()) {
            return hirepoint
                    .map(hirepointMapping::entityToDto);
        }
        return Optional.empty();
    }

    @Override
    public Optional<HirePointDto> findByEmail(String email) {
        Optional<HirePoint> hirepoint = hirePointRepository.findByEmailIgnoreCase(email);
        if (hirepoint.isPresent()) {
            return hirepoint
                    .map(hirepointMapping::entityToDto);
        }
        return Optional.empty();
    }
}
