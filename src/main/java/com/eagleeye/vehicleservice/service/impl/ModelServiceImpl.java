package com.eagleeye.vehicleservice.service.impl;

import com.eagleeye.vehicleservice.document.Model;
import com.eagleeye.vehicleservice.mapping.impl.ModelMapping;
import com.eagleeye.vehicleservice.repository.ModelRepository;
import com.eagleeye.vehicleservice.service.ModelService;
import commonentity.vehicle.ModelDto;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ModelServiceImpl implements ModelService {
    private final ModelRepository modelRepository;
    private final ModelMapping modelMapping;

    public ModelServiceImpl(ModelRepository modelRepository,
                            ModelMapping modelMapping) {
        this.modelRepository = modelRepository;
        this.modelMapping = modelMapping;
    }

    @Override
    public Optional<ModelDto> addNewModel(ModelDto modelDto) {
        Model model = modelRepository.save(modelMapping.dtoToEntity(modelDto));
        if (model != null) {
            return Optional.of(modelMapping.entityToDto(model));
        }
        return Optional.empty();
    }

    @Override
    public Optional<ModelDto> findById(String id) {
        Optional<Model> model = modelRepository.findById(id);
        if (model.isPresent()) {
            return Optional.of(modelMapping.entityToDto(model.get()));
        }
        return Optional.empty();
    }

    @Override
    public List<ModelDto> modelByManufactureId(String id) {
        List<Model> models = modelRepository.findByManufacturer_Id(id);
        if (models != null) {
            return models
                    .stream()
                    .map(modelMapping::entityToDto)
                    .collect(Collectors.toList());
        }
        return null;
    }

    @Override
    public List<ModelDto> modelByManufacturerName(String manufactureName) {
        List<Model> models = modelRepository.findByManufacturer_ManufacturerNameIgnoreCase(manufactureName);
        if (models != null) {
            return models
                    .stream()
                    .map(modelMapping::entityToDto)
                    .collect(Collectors.toList());
        }
        return null;
    }

    @Override
    public Optional<ModelDto> updateModel(String modelId, ModelDto modelDto) {
        return Optional.empty();
    }

    @Override
    public Optional<ModelDto> findByModelName(String modelName) {
        Optional<Model> model = modelRepository.findByModelNameIgnoreCase(modelName);
        if(model.isPresent()) {
            return Optional.of(modelMapping.entityToDto(model.get()));
        }
        return Optional.empty();
    }

    @Override
    public List<ModelDto> findAll() {
        return modelRepository
                .findAll()
                .stream()
                .map(modelMapping::entityToDto)
                .collect(Collectors.toList());
    }
}
