package com.eagleeye.vehicleservice.service.impl;

import com.eagleeye.vehicleservice.document.Vehicle;
import com.eagleeye.vehicleservice.mapping.impl.VehicleMapping;
import com.eagleeye.vehicleservice.repository.VehicleRepository;
import com.eagleeye.vehicleservice.service.VehicleService;
import commonentity.vehicle.VehicleDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class VehicleServiceImpl implements VehicleService {
    private final VehicleRepository vehicleRepository;
    private final VehicleMapping vehicleMapping;

    @Autowired
    public VehicleServiceImpl(VehicleRepository vehicleRepository,
                              VehicleMapping vehicleMapping) {
        this.vehicleRepository = vehicleRepository;
        this.vehicleMapping = vehicleMapping;
    }

    @Override
    public VehicleDto addNew(VehicleDto vehicleDto) {
        Vehicle vehicle = vehicleRepository.save(vehicleMapping.dtoToEntity(vehicleDto));
        return vehicleMapping.entityToDto(vehicle);
    }

    @Override
    public Optional<VehicleDto> findVehicleByPlateNumber(String vehiclePlateNumber) {
        Optional<Vehicle> entity = vehicleRepository.findByPlateNumberIgnoreCase(vehiclePlateNumber);
        if (entity.isPresent()) {
            return Optional.of(vehicleMapping.entityToDto(entity.get()));
        }
        return Optional.empty();
    }

    @Override
    public Optional<VehicleDto> findById(String id) {
        Optional<Vehicle> vehicle = vehicleRepository.findById(id);
        if (vehicle.isPresent()) {
            return Optional.of(vehicleMapping.entityToDto(vehicle.get()));
        }
        return Optional.empty();
    }

    @Override
    public List<VehicleDto> vehicles() {
        List<Vehicle> vehicleList = vehicleRepository.findAll();
        if(vehicleList.size() > 0) {
            return vehicleList
                    .stream()
                    .map(vehicleMapping::entityToDto)
                    .collect(Collectors.toList());
        }
        return null;
    }


}
