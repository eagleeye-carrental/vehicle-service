package com.eagleeye.vehicleservice.service.impl;

import com.eagleeye.vehicleservice.document.Manufacturer;
import com.eagleeye.vehicleservice.mapping.impl.ManufacturerMapping;
import com.eagleeye.vehicleservice.repository.ManufacturerRepository;
import com.eagleeye.vehicleservice.service.ManufacturerService;
import commonentity.vehicle.ManufacturerDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class manufacturerServiceImpl implements ManufacturerService {
    private final ManufacturerRepository manufacturerRepository;
    private final ManufacturerMapping manufacturerMapping;

    @Autowired
    public manufacturerServiceImpl(ManufacturerRepository manufacturerRepository,
                                   ManufacturerMapping manufacturerMapping) {
        this.manufacturerRepository = manufacturerRepository;
        this.manufacturerMapping = manufacturerMapping;
    }

    @Override
    public Optional<ManufacturerDto> addManufacturer(ManufacturerDto dto) {
        Manufacturer manufacturer = manufacturerRepository
                .insert(manufacturerMapping.dtoToEntity(dto));
        if (manufacturer != null) {
            return Optional.of(manufacturerMapping
                    .entityToDto(manufacturer));
        }
        return Optional.empty();
    }

    @Override
    public Optional<ManufacturerDto> findById(String id) {
        Optional<Manufacturer> manufacturer = manufacturerRepository.findById(id);
        if (manufacturer.isPresent()) {
            return Optional.of(manufacturerMapping
                    .entityToDto(manufacturer.get()));
        }
        return Optional.empty();
    }

    @Override
    public Optional<ManufacturerDto> findByName(String name) {
        Optional<Manufacturer> manufacturer = manufacturerRepository.findByManufacturerNameIgnoreCase(name);
        if (manufacturer.isPresent()) {
            return Optional.of(manufacturerMapping.entityToDto(manufacturer.get()));
        }
        return Optional.empty();
    }

    @Override
    public Optional<ManufacturerDto> updateManufacturer(String id, ManufacturerDto dto) {
        return Optional.empty();
    }

    @Override
    public void deleteManufacturer(String id) {

    }

    @Override
    public List<ManufacturerDto> manufacturers() {
        List<Manufacturer> manufacturers = manufacturerRepository.findAll();
        if (manufacturers.size() > 0) {
            return manufacturers
                    .stream()
                    .map(manufacturerMapping::entityToDto)
                    .collect(Collectors.toList());
        }
        return null;
    }
}
