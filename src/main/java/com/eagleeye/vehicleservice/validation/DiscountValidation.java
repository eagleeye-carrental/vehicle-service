package com.eagleeye.vehicleservice.validation;

import com.eagleeye.vehicleservice.errorDto.DiscountError;
import com.eagleeye.vehicleservice.service.DiscountService;
import commonentity.vehicle.DiscountDto;
import org.springframework.stereotype.Component;

@Component
public class DiscountValidation {
    private final DiscountService discountService;
    private DiscountError discountError;

    public DiscountValidation(DiscountService discountService) {
        this.discountService = discountService;
    }

    public DiscountError discountValidation(DiscountDto discountDto) {
        discountError = new DiscountError();
        boolean valid = true;

        valid = checkDiscountPercentage(discountDto) && valid;
        valid = checkDiscountCode(discountDto) && valid;
        valid = checkDiscountValidity(discountDto) && valid;

        discountError.setValid(valid);
        return discountError;
    }

    private boolean checkDiscountValidity(DiscountDto discountDto) {
        if (discountDto.getDiscountValidity() == null) {
            discountError.setDiscountValidity("Set discount validity period");
            return false;
        }
        return true;
    }

    private boolean checkDiscountCode(DiscountDto discountDto) {
        if (discountDto.getDiscountCode() == null
                ||
                discountDto.getDiscountCode().isEmpty()) {
            discountError.setDiscountCode("Discount code required");
            return false;
        }
        return true;
    }


    private boolean checkDiscountPercentage(DiscountDto discountDto) {
        if (discountDto.getDiscountPercentage() < 0) {
           discountError.setDiscountPercentage("Discount percentage should greater than 0");
            return false;
        }
        return true;
    }
}
