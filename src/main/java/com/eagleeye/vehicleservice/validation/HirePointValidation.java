package com.eagleeye.vehicleservice.validation;

import com.eagleeye.vehicleservice.errorDto.HirePointError;
import com.eagleeye.vehicleservice.service.HirePointService;
import commonentity.vehicle.HirePointDto;
import org.springframework.stereotype.Component;

@Component
public class HirePointValidation {
    private final HirePointService hirePointService;
    HirePointError hirePointError = null;

    public HirePointValidation(HirePointService hirePointService) {
        this.hirePointService = hirePointService;
    }

    public HirePointError validator(HirePointDto hirePointDto) {
        hirePointError = new HirePointError();
        boolean isValid = true;
        isValid = checkEmail(hirePointDto) && isValid;
        isValid = checkMobile(hirePointDto) && isValid;
        hirePointError.setValid(isValid);
        return hirePointError;

    }

    private boolean checkMobile(HirePointDto hirePointDto) {
        if (hirePointService.findByMobile(hirePointDto.getMobileNumber()).isPresent()) {
            hirePointError.setPhone("Mobile Number Already Exists!");
            return false;
        }
        return true;
    }

    private boolean checkEmail(HirePointDto hirePointDto) {
        if (hirePointService.findByEmail(hirePointDto.getEmail()).isPresent()) {
            hirePointError.setEmail("Email Already Exists!");
            return false;
        }
        return true;
    }
}
