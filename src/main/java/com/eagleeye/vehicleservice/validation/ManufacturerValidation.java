package com.eagleeye.vehicleservice.validation;

import com.eagleeye.vehicleservice.errorDto.ManufacturerError;
import com.eagleeye.vehicleservice.service.ManufacturerService;
import commonentity.vehicle.ManufacturerDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ManufacturerValidation {
    private final ManufacturerService manufacturerService;
    private ManufacturerError manufacturerError = null;

    @Autowired
    public ManufacturerValidation(ManufacturerService manufacturerService) {
        this.manufacturerService = manufacturerService;
    }

    public ManufacturerError validation(ManufacturerDto dto) {
        manufacturerError = new ManufacturerError();
        boolean isValid = true;
        isValid = checkManufacturer(dto) && isValid;
        manufacturerError.setValid(isValid);
        return manufacturerError;
    }

    private boolean checkManufacturer(ManufacturerDto dto) {
        if (manufacturerService.findByName(dto.getManufacturerName()).isPresent()) {
            manufacturerError.setManufacturer("Manufacturer Already Exists!");
            return false;
        }
        return true;
    }
}
