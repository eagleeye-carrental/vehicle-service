package com.eagleeye.vehicleservice.validation;

import com.eagleeye.vehicleservice.errorDto.ModelError;
import com.eagleeye.vehicleservice.service.ModelService;
import commonentity.vehicle.ModelDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ModelValidation {
    private final ModelService modelService;
    private ModelError error = null;

    @Autowired
    public ModelValidation(ModelService modelService) {
        this.modelService = modelService;
    }

    public ModelError validation(ModelDto dto) {
        error = new ModelError();
        boolean valid = true;

        valid = checkModelName(dto) && valid;

        error.setValid(valid);
        return error;
    }

    private boolean checkModelName(ModelDto dto) {
        if (modelService.findByModelName(dto.getModelName()).isPresent()) {
            error.setModelName("Model name already exists!");
            return false;
        }
        return true;
    }
}
