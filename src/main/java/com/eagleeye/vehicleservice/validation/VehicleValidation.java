package com.eagleeye.vehicleservice.validation;

import com.eagleeye.vehicleservice.errorDto.VehicleError;
import com.eagleeye.vehicleservice.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class VehicleValidation {
    private final VehicleService vehicleService;
    private VehicleError error = null;

    @Autowired
    public VehicleValidation(VehicleService vehicleService) {
        this.vehicleService = vehicleService;
    }
    public VehicleError validator(String vehiclePlateNumber) {
        error = new VehicleError();
        boolean isValid = true;
        isValid = checkVehilePlateNumber(vehiclePlateNumber) && isValid;
        error.setValid(isValid);
        return error;
    }

    private boolean checkVehilePlateNumber(String vehiclePlateNumber) {
        if(vehicleService.findVehicleByPlateNumber(vehiclePlateNumber).isPresent()) {
            error.setPlateNumber("plate Number already exists!");
            return false;
        }
        return true;
    }
}
